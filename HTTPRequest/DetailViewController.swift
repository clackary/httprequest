//
//  DetailViewController.swift
//  HTTPRequest
//
//  Created by Zachary Cole on 3/3/16.
//  Copyright © 2016 Zachary Cole. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    var names = []
    var tv = UITableView()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tv = UITableView.init(frame: CGRectMake(0, 0, view.bounds.size.width, view.bounds.size.height), style: UITableViewStyle.Plain)

        tv.dataSource = self
        tv.delegate = self
        tv.registerClass(UITableViewCell.self, forCellReuseIdentifier: "reuseIdentifier")

        self.view.addSubview(tv)
        
        get()
        
        tv.reloadData()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        tv.reloadData()
    }
    
    func get() {
        let url:NSURL = NSURL(string: "https://adamsparsetest.herokuapp.com/parse/classes/Game")!
        let session = NSURLSession.sharedSession()
        
        let request = NSMutableURLRequest(URL: url)
        request.HTTPMethod = "GET"
        request.cachePolicy = NSURLRequestCachePolicy.UseProtocolCachePolicy
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("adamsappid", forHTTPHeaderField: "X-Parse-Application-ID")
        
        let task = session.dataTaskWithRequest(request) { (data, response, error) -> Void in
            //populate array
//            let responseString = NSString.init(data: data!, encoding: NSUTF8StringEncoding)
            //reload data
            do {
                var json = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers) as? Dictionary <String, AnyObject>
                self.names = (json!["results"] as? Array<AnyObject>)!
                dispatch_async(dispatch_get_main_queue()) {
                    self.tv.reloadData()
                }
            }
            catch {
                
            }
            NSLog("test")
        }
        
        task.resume()
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return names.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath)

        let d : NSDictionary =  names[indexPath.row] as! NSDictionary
        
        var s = (d["name"] as? String)! + ", "
        s.appendContentsOf( (d["color"] as? String)!)
        cell.textLabel?.text = s
        
        cell.detailTextLabel?.text = d["color"] as? String
        
        return cell
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
