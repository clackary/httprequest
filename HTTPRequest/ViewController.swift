//
//  ViewController.swift
//  HTTPRequest
//
//  Created by Zachary Cole on 3/3/16.
//  Copyright © 2016 Zachary Cole. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var colorField: UITextField!
    @IBOutlet weak var nameField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func buttonTouched(sender: AnyObject) {
        post(nameField.text!, color: colorField.text!)
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func post(name: String, color: String) {
        let json = [ "name": name , "color": color]
        
        do {
            let jsonData = try NSJSONSerialization.dataWithJSONObject(json, options: .PrettyPrinted)
            let url:NSURL = NSURL(string: "https://adamsparsetest.herokuapp.com/parse/classes/Game")!
            let request = NSMutableURLRequest(URL: url, cachePolicy: NSURLRequestCachePolicy.UseProtocolCachePolicy, timeoutInterval: 60)
            request.HTTPMethod = "POST"
            
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("adamsappid", forHTTPHeaderField: "X-Parse-Application-ID")
            request.HTTPBody = jsonData
            
            
            let session = NSURLSession.sharedSession().dataTaskWithRequest(request){ data, response, error in
                if error != nil{
                    print("Error -> \(error)")
                    return
                }
                
                do {
                    let result = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? [String:AnyObject]
                    
                    print("Result -> \(result)")
                    
                } catch {
                    print("Error -> \(error)")
                }
            }
            
            session.resume()
            
        } catch {
            //print error
        }
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

